//Image array for the reels
const reels = ["images/alien1.png",
                "images/alien1.png",
                "images/galaxy1.png",
                "images/lasergun.png",
                "images/mib.png",
                "images/ufo1.png",
                "images/ufo2.png",
                "images/ufo3.png",
]

//Image array for hovering ufo
const hovering = ["images/hovering_1.png",
"images/hovering_2.png",
"images/hovering_3.png",
"images/hovering_4.png",
"images/hovering_5.png",
"images/hovering_6.png",
"images/hovering_7.png",
"images/hovering_8.png",
]

//retrieves the pic source for comparison so wins can be set at different values for different pictures
let alienValue = document.getElementById("alienValue").src
let galaxyValue = document.getElementById("galaxyValue").src
let laserGunValue = document.getElementById("lasergunValue").src
let mibValue = document.getElementById("mibValue").src
let ufo1Value = document.getElementById("ufo1Value").src
let ufo2Value = document.getElementById("ufo2Value").src
let ufo3Value = document.getElementById("ufo3Value").src

//values of final pictures after spin
let valueOne
let valueTwo
let valueThree

//Starting credits
let totalCredits = 1000


//Adds or subtracts credits based on bet or win
function creditSum(){
let credits = document.getElementById("credits")
let creditsNum = document.createTextNode(totalCredits)
credits.appendChild(creditsNum)
}
creditSum()


//Spins reel one
function randomPicOne() {
    let randomPic1 = Math.floor(Math.random() * reels.length)
    document.getElementById("reelOne").src = reels[randomPic1]
    return reels[randomPic1]
}
randomPicOne()

//Spins reel two
function randomPicTwo() {
    let randomPic2 = Math.floor(Math.random() * reels.length)
    document.getElementById("reelTwo").src = reels[randomPic2] 
}
randomPicTwo()

//Spins reel three
function randomPicThree() {
    let randomPic3 = Math.floor(Math.random() * reels.length)
    document.getElementById("reelThree").src = reels[randomPic3] 
}
randomPicThree()

//count for the setInterval
let counter = 0


//Alerts of win or loss
function winner(value){
    let win = document.getElementById("win")
    let winText = document.createTextNode(value)
    
    win.appendChild(winText)
}

//Slot operational functions
let button = document.getElementById("spin")
button.onclick = function (){


    button.disabled = true

    

    document.getElementById("credits").innerHTML = ""
    totalCredits += -75
    creditSum()
    document.getElementById("win").innerHTML = ""

    let randomOne = setInterval (function(){
        
            counter++
            randomPicOne()

            if (counter > 30){
                clearInterval(randomOne)
            }
            valueOne = document.getElementById("reelOne").src
            
    } , 100)

    let randomTwo = setInterval (function(){
        
        counter++
        randomPicTwo()

        if (counter > 45){
            clearInterval(randomTwo)
        }
        valueTwo = document.getElementById("reelTwo").src
        
    } , 100)

    let randomThree = setInterval (function(){
        
        counter++
        randomPicThree()

        if (counter > 60){
            counter = 0
            clearInterval(randomThree)
        valueThree = document.getElementById("reelThree").src
        winOrLose()
        button.disabled = false
        }
    } , 100)

    
    //refills the credits when to low
        if(totalCredits < 74){
            alert("WoW YOU NEED MORE CREDITS, HERE YA GO!")
            document.getElementById("credits").innerHTML = ""
            totalCredits += 1000
            creditSum()
        }

        
    
}

//Designates wins, prizes and losses
function winOrLose(){
    
    if(valueOne === alienValue && valueTwo === alienValue && valueThree === alienValue){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 500
        creditSum()
        return winner("YOU WIN: 500")
    } else if (valueOne === alienValue && valueTwo == alienValue){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 300
        creditSum()
        return winner("YOU WIN: 300")
    } else if (valueOne === mibValue && valueTwo === mibValue && valueThree === mibValue){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 400
        creditSum()
        return winner("YOU WIN: 400")
    } else if (valueOne === mibValue && valueTwo === mibValue){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 200
        creditSum()
        return winner("YOU WIN: 200")
    } else if (valueOne === laserGunValue && valueTwo === laserGunValue && valueThree === laserGunValue){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 300
        creditSum()
        return winner("YOU WIN: 300")
    } else if (valueOne === laserGunValue && valueTwo === laserGunValue){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 150
        creditSum()
        return winner("YOU WIN: 150")
    } else if (valueOne === galaxyValue && valueTwo === galaxyValue && valueThree === galaxyValue){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 200
        creditSum()
        return winner("YOU WIN: 200")
    } else if (valueOne === galaxyValue && valueTwo === galaxyValue){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 100
        creditSum()
        return winner("YOU WIN: 100")
    } else if (valueOne === ufo1Value && valueTwo === ufo1Value && valueThree === ufo1Value){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 100
        creditSum()
        return winner("YOU WIN: 100")
    } else if (valueOne === ufo1Value && valueTwo === ufo1Value){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 50
        creditSum()
        return winner("YOU WIN: 50")
    } else if (valueOne === ufo2Value && valueTwo === ufo2Value && valueThree === ufo2Value){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 100
        creditSum()
        return winner("YOU WIN: 100")
    } else if (valueOne === ufo2Value && valueTwo === ufo2Value){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 50
        creditSum()
        return winner("YOU WIN: 50")
    } else if (valueOne === ufo3Value && valueTwo === ufo3Value && valueThree === ufo3Value){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 100
        creditSum()
        return winner("YOU WIN: 100")
    } else if (valueOne === ufo3Value && valueTwo === ufo3Value){
        document.getElementById("credits").innerHTML = ""
        totalCredits += 50
        creditSum()
        return winner("YOU WIN: 50")
    } else {
        return winner("YOU LOSE")
    }
}



let counter2 = 0
let ufoPics = 0
let hoveringUfo = setInterval (function(){
    counter2 ++
    document.getElementById("hoveringufo").src = hovering[ufoPics]

    if (counter2 > 0){
        
            if(ufoPics < 7){
            ufoPics++
        } else if (ufoPics = 7){
            ufoPics = 0
            counter2 = 0
        
            
        }

    } 
    
}, 150)

